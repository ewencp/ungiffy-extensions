var get_ungiffy_base_url = function() {
  var ungiffy_url = window.ungiffy_base_url;
  if (ungiffy_url === undefined)
    ungiffy_url = 'http://ungiffy.com/';
  return ungiffy_url;
};

// Check if a URL is an ungiffy URL, which then shouldn't be further
// processed.
var is_ungiffy_url = function(url) {
  var ungiffy_url = get_ungiffy_base_url();
  // For now, if it contains ungiffy.com at all
  return (url.indexOf(ungiffy_url) != -1);
};

var gen_ungiffy_url = function(url, type, fmt) {
  var new_url = url;
  // Dump the protocol section
  new_url = new_url.replace('http://', '');
  // URI encode
  new_url = encodeURI(new_url);
  // Do extra / escaping. Global flag on regex to replace all
  // instances.
  new_url = new_url.replace(/\//g, '~');

  // Prepend ungiffy domain
  var ungiffy_url = get_ungiffy_base_url();
  new_url = ungiffy_url + type + '/' + new_url;
  if (fmt !== undefined) new_url = new_url + '/' + fmt;
  console.log('Redirecting ' + url + ' to ' + new_url);
  return new_url;
};
