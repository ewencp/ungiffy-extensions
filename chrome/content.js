// The content.js version is for in-page elements. We need to actually swap
// these entirely with a new element because we want to display a video tag
// instead of just a separate image. To do this, properly, we need to muck with
// the DOM before any loading occurs.

var randID = function() {
  return Math.random().toString(36).substring(7);
};

var ignoreGifs = {};

var replaceGifs = function(event) {
  var $elem = $(event.srcElement);
  if (event.srcElement.tagName == 'IMG' && $elem.attr('src').indexOf('.gif') != -1) {
    var src = $elem.attr('src');
    // We may be ignoring this gif, e.g. if it was double clicked to get
    // original.
    if (ignoreGifs[src] !== undefined)
      return;
    var new_src = gen_ungiffy_url(src, 'v');
    console.log('replace ' + src + ' with ' + new_src);

    var div_id = randID();
    $elem.replaceWith('<div id="' + div_id + '"></div>');

    var restore_original = function() {
      console.log("restoring img for " + src);
      $(this).replaceWith(
        $('<img src="' + src + '"></img>').dblclick(function() {
          console.log("unrestoring img for " + src);
          delete ignoreGifs[src];
          replaceGifs({'srcElement' : this});
        })
      );
    };

    $.ajax(
      new_src,
      {
        'dataType' : 'html',
      }
    ).done(
      function(html) {
        ignoreGifs[src] = true;

        var new_img = $(html).dblclick(restore_original);
        $('#' + div_id).replaceWith(new_img);

        // Also make a simple request that tests for whether this . The other
        // requests for the actual video will also fail, but we can't seem to
        // get at those errors, so instead we use this.
        $.getJSON(
          gen_ungiffy_url(src, 'r', 'unanimated'),
          function(data) {
            if (data.unanimated) restore_original.apply(new_img);
          }
        );
      }
    );
  }

};

document.addEventListener('beforeload', replaceGifs, true);