ungiffy Extensions
==================

These are browser extensions for ungiffy. They try to replace
gif images in pages (and standalone files) with ungiffy
videos. Currently we only have a Chrome/Chromium extension.

Setup
-----

You can override settings in chrome/config.js as
configuration. Currently this just points at the appropriate
server. If you don't provide any settings the default will be used.
